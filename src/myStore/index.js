import { reactive } from 'vue'

const state = reactive({
  usePageTransition: false,
  iosBrowserSwipingBack: false
})

const getters = {
 //
}

const store = {
  state,
  getters
}

export default store