import { LocalStorage, Notify } from 'quasar'
import store from '../../store/index'

import { api } from '../../boot/axios'
import { routerInstance } from '../../boot/router'

export default {

  /**
     * Login a user with supplied credentials
     *
     * @param {object} credentials containing email and password
     * @param {string} redirect where to redirect after login
     */
  async register (credentials) {
    let url = '/register'
    await api.post(url, credentials)
      .then((response) => {
        // let data = response.data.data
        // let user = data.user
        // this.setLoggedUser(data)
        // api.defaults.headers.common['Authorization'] = 'Bearer ' + data.token
        routerInstance.push('/verify-account')
      }).catch(() => {
        Notify.create({
          message: 'Please enter valid email address',
          position: 'bottom',
          timeout: 3000,
          color: 'negative',
          icon: 'warning'
        })
      })
  },

    /**
     * Login a user with supplied credentials
     *
     * @param {object} credentials containing email and password
     * @param {string} redirect where to redirect after login
     */
     async login (credentials) {
      let url = '/login'
      await api.post(url, credentials)
        .then((response) => {
          let data = response.data.data
          let user = data.user
          this.setLoggedUser(data)
          api.defaults.headers.common['Authorization'] = 'Bearer ' + data.token
          Notify.create({
            type: 'positive',
            message: 'Welcome back.'
          })
          routerInstance.push('/dashboard')
        }).catch(() => {
          Notify.create({
            message: 'The login access link is invalid',
            position: 'bottom',
            timeout: 3000,
            color: 'negative',
            icon: 'warning'
          })
          routerInstance.push('/')
        })
    },

  /**
     * Logout current User and clear web storage
     *
     */
  logout (version = null) {
    try {
      this.removeAuthLocalStorage()
        routerInstance.push('/')
    } catch (error) {
      console.log(error)
      routerInstance.push('/')
    }
  },
  removeAuthLocalStorage () {
    LocalStorage.remove('user')
  },
  /**
   * Check the Authentication state of the current User
   *
  */
  async checkAuth () {
    let user = LocalStorage.getItem('user')
    if (user) {
        if (!this.validateLoggedinUser()) {
          this.logout()
        }
        return true
      
    } else {
      this.logout()
    }
  },
  async checkAuth () {
    if (this.validateToken()) {
      await api.post('auth/status')
        .then((response) => {
          return true
        }).catch((error) => {
          // console.log(error)
          this.logout()
        })
    }else{
      this.logout()
    }
    
      
    
  },

    /**
     * Exchange the currently stored token for a refreshed token
     *
     */
     async validateToken () {
      let user = LocalStorage.getItem('user')
      if (user) {
        api.defaults.headers.common['Authorization'] = 'Bearer ' + user.token
        return true
      } else {
        return false
      }
    },

  /**
     * Get the Authenticated Users credentials
     *
     */
  setAuthUser (user) {
    LocalStorage.set('user', user)
    store.state.loginUser = user
  },
  /**
     * Get the Authenticated Users credentials
     *
     */
  getAuthUser (user) {
    return LocalStorage.getItem('user')
  },


  /**
     * Set logged in user store
     *
     */
  setLoggedUser (data) {
    LocalStorage.set('user', data)
    return true
  },

}
