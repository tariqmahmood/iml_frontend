import { boot } from 'quasar/wrappers'
import store from 'src/myStore'
import _ from 'lodash'

import auth from '../boot/ilm/auth'

let routerInstance = null

export default boot(({ router }) => {

  routerInstance = router


/**
 * Assign Auth protected routes
 */
 router.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.public)) {
    return next()
  }
  //let roleId = LocalStorage.getItem('_client') ? JSON.parse(atob(LocalStorage.getItem('_client'))).user.role_id : null

  let roleBased = false
  if (!_.isEmpty(to.meta) && !to.meta.public) {
    auth.checkAuth()
    next()
  } else {
    return next('/')
  }
})

  // router.afterEach((to, from) => {
  //   let fromRootPath = `/${ from.path.split('/')[1] }`,
  //       toRootPath = `/${ to.path.split('/')[1] }`

  //   // if we navigated to a different 'section'
  //   if (fromRootPath !== toRootPath) {
  //     // don't use page transition
  //     store.state.usePageTransition = false
  //   }
  //   // we navigated to the same section
  //   else {
  //     // use page transition
  //     store.state.usePageTransition = true

  //     if (from.path === to.path && to.path !== toRootPath) {
  //       router.push(toRootPath)
  //     }
  //   }
    
  //   // update to property on nav item, whenever we change route
  //   updateNavItem()

  //   function updateNavItem() {
  //     // const navItemIndex = store.state.navItems.findIndex(navItem => navItem.root === toRootPath)
  //     // store.state.navItems[navItemIndex].to = to.path
  //   }

  // })
})

export { routerInstance }
