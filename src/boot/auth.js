import auth from 'src/boot/ematix/auth.js'

export default ({ app, router, store, Vue }) => {
  Vue.prototype.$auth = auth
}
