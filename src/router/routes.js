
const routes = [
  {
    path: '/',
    component: () => import('layouts/AuthLayout.vue'),
    meta: { public: true },
    children: [
      { path: '', component: () => import('src/pages/Home.vue') },
      { path: '/login/:token', component: () => import('pages/Login.vue') },
      { path: '/verify-account', component: () => import('pages/VerifyAccount.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    meta: { public: false },
    children: [
      { path: '/dashboard', component: () => import('pages/Dashboard.vue') },
      { path: '/profile', component: () => import('pages/Profile.vue') },
      { path: '/settings', component: () => import('pages/Setting.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    meta: { public: true },
    component: () => import('pages/Error404.vue')
  }
]

export default routes
