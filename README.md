#  Test Project  ( Frontend )

A Quasar Framework app



## Quasar Framework Setup 
```bash
# Node.js >=12.22.1 is required.

$ yarn global add @quasar/cli
# or
$ npm install -g @quasar/cli



# TIP
# If you are using Yarn, make sure that the Yarn global install location is in your PATH:
# in ~/.bashrc or equivalent
export PATH="$(yarn global bin):$PATH"

```

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev

# quasar dev will run dev server at : http://localhost:8080
# To work this we need to setup iml_api project as wiil to clone 
# iml_api repo : git clone https://tariqmahmood@bitbucket.org/tariqmahmood/iml_api.git
# quasar dev user api endpoint at : http://127.0.0.1:8000 
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```
